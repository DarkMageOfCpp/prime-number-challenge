/*
 * main.cpp
 *
 *  Created on: 17/ott/2014
 *      Author: DarkMageOfCpp
 */

#include "mymath.h"
#include <cstdlib>

int main()
{
	long long int num = 0;

	std::cout << "Insert a number to check if it is a prime number: \n \n";

	std::cin >> num;

	if(isPrimeNumber(num))
		std::cout << "The number you inserted is prime! \n";

	system("pause>>null");

	return 0;
}
