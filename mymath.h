/*
 * mymath.h
 *
 *  Created on: 17/ott/2014
 *      Author: DarkMageOfCpp
 */

#ifndef MYMATH_H_
#define MYMATH_H_

#include <iostream>

bool isPrimeNumber(long long int num)
{
	std::cout << "Argument number is divisible for: \n\n";

	if(num % 2 != 0 && num % 3 != 0 && num % 5 != 0 && num % 7 != 0)
	{

		std::cout << "nothing! \n\n";
		return true;

	} else {

		if(num % 2 == 0)

			std::cout << 2 << std::endl;

		if(num % 3 == 0)

			std::cout << 3 << std::endl;

		if(num % 5 == 0)

			std::cout << 5 << std::endl;

		if(num % 7 == 0)

			std::cout << 7 << std::endl;

		return false;
	}
}



#endif /* MYMATH_H_ */
